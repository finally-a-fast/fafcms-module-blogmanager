<?php

namespace fafcms\blogmanager;

use fafcms\helpers\abstractions\PluginModule;

class Module extends PluginModule
{
    public $controllerNamespace = 'fafcms\blogmanager\controllers';
    public $gridView = [];
    public $actionColumnClass = 'yiiui\yii2advancedgridview\grid\columns\ActionColumn';
    public $actionColumn;
}
