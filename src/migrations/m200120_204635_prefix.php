<?php

namespace fafcms\blogmanager\migrations;

use fafcms\blogmanager\models\Article;
use yii\db\Migration;

/**
 * Class m200120_204635_prefix
 * @package fafcms\blogmanager\migrations
 */
class m200120_204635_prefix extends Migration
{
    public function safeUp()
    {
        $this->renameTable('{{%article}}', Article::tableName());
    }

    public function safeDown()
    {
        $this->renameTable(Article::tableName(), '{{%article}}');
    }
}
