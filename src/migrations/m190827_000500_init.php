<?php

namespace fafcms\blogmanager\migrations;

use yii\db\Migration;

/**
 * Class m190827_000500_init
 * @package fafcms\blogmanager\migrations
 */
class m190827_000500_init extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%article}}', [
            'id' => $this->primaryKey(10)->unsigned(),
            'status' => $this->string(255)->notNull()->defaultValue('inactive'),
            'display_start' => $this->datetime()->null()->defaultValue(null),
            'display_end' => $this->datetime()->null()->defaultValue(null),
            'site_id' => $this->integer(10)->unsigned()->notNull(),
            'headline' => $this->string(255)->notNull(),
            'subheadline' => $this->string(255)->null()->defaultValue(null),
            'publication' => $this->datetime()->notNull(),
            'media' => $this->string(255)->null()->defaultValue(null),
            'summary' => $this->text()->null()->defaultValue(null),
            'text' => $this->text()->null()->defaultValue(null),
            'created_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'updated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'activated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'deactivated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'deleted_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'created_at' => $this->datetime()->null()->defaultValue(null),
            'updated_at' => $this->datetime()->null()->defaultValue(null),
            'activated_at' => $this->datetime()->null()->defaultValue(null),
            'deactivated_at' => $this->datetime()->null()->defaultValue(null),
            'deleted_at' => $this->datetime()->null()->defaultValue(null),
        ], $tableOptions);

        $this->createIndex('idx-article-created_by', '{{%article}}', ['created_by'], false);
        $this->createIndex('idx-article-updated_by', '{{%article}}', ['updated_by'], false);
        $this->createIndex('idx-article-activated_by', '{{%article}}', ['activated_by'], false);
        $this->createIndex('idx-article-deactivated_by', '{{%article}}', ['deactivated_by'], false);
        $this->createIndex('idx-article-deleted_by', '{{%article}}', ['deleted_by'], false);
        $this->createIndex('idx-article-site_id', '{{%article}}', ['site_id'], false);

        $this->addForeignKey('fk-article-site_id', '{{%article}}', 'site_id', '{{%site}}', 'id', 'RESTRICT', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk-article-site_id', '{{%article}}');
        $this->dropTable('{{%article}}');
    }
}
