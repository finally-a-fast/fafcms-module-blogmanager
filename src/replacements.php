<?php
use fafcms\sitemanager\models\Snippet;
use fafcms\blogmanager\models\Article;
use fafcms\sitemanager\models\ContentmetaTopic;
use fafcms\parser\component\Parser;
use fafcms\settingmanager\Bootstrap as SettingmanagerBootstrap;

return [
    'blog-posts' => [
        'replacement' => function($type, $parentTagName, $node, $crawler, $data, $language) {
            $snippetId = $node->hasAttribute('snippet')?$node->getAttribute('snippet'):Yii::$app->getModule(SettingmanagerBootstrap::$id)->getSetting('blog_post_default_snippet', null);
            $limit = $node->hasAttribute('limit')?$node->getAttribute('limit'):Yii::$app->getModule(SettingmanagerBootstrap::$id)->getSetting('blog_post_default_limit', 9999); //todo
            $topic = $node->hasAttribute('topic')?$node->getAttribute('topic'):null;

            if ($topic === null) {
                $topicChildren = $crawler->filterXPath(Yii::$app->fafcmsParser->name.'-'.$parentTagName.'/'.Yii::$app->fafcmsParser->name.'-'.$parentTagName.'-topic');

                if (count($topicChildren) > 0) {
                    $topic = Yii::$app->fafcmsParser->fullTrim(Yii::$app->fafcmsParser->parse($type, $topicChildren->html(), $parentTagName, $data, $language));
                }
            }

            if ($snippetId === null) {
                return '';
            }

            $snippet = Snippet::getContentById($snippetId);

            if ($snippet === null) {
                return '';
            }

            $article = Article::find();

            if ($topic !== null) {
                $article->innerJoinWith('contentmeta.contentmetaTopics')->where([
                    ContentmetaTopic::tableName().'.topic_id' => explode(',', $topic)
                ]);
            }

            $data['articles'] = $article->limit($limit)->orderBy('publication DESC')->all();

            return Yii::$app->fafcmsParser->parse($type, $snippet, $parentTagName, $data, $language);
        },
        'allowedTypes' => [Parser::TYPE_PAGE]
    ],
];
