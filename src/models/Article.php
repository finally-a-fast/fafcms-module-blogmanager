<?php

namespace fafcms\blogmanager\models;

use fafcms\fafcms\inputs\DateTimePicker;
use fafcms\fafcms\inputs\DropDownList;
use fafcms\fafcms\inputs\ExtendedDropDownList;
use fafcms\fafcms\inputs\TinyMce;
use fafcms\fafcms\inputs\TextInput;
use fafcms\fafcms\inputs\AceEditor;
use fafcms\blogmanager\Bootstrap;
use fafcms\filemanager\inputs\FileSelect;
use fafcms\helpers\interfaces\ContentmetaInterface;
use fafcms\helpers\interfaces\EditViewInterface;
use fafcms\helpers\interfaces\FieldConfigInterface;
use fafcms\helpers\interfaces\IndexViewInterface;
use fafcms\helpers\traits\BeautifulModelTrait;
use fafcms\helpers\traits\HashIdPrimaryKeyTrait;
use fafcms\helpers\traits\TagTrait;
use fafcms\helpers\traits\OptionTrait;
use fafcms\helpers\validators\HashIdValidator;
use Yii;
use DateTime;
use DateTimeZone;
use fafcms\helpers\ActiveRecord;
use yii\validators\DateValidator;
use fafcms\fafcms\queries\DefaultQuery;
use fafcms\sitemanager\models\Site;
use fafcms\sitemanager\traits\ContentmetaTrait;
use fafcms\settingmanager\Bootstrap as SettingmanagerBootstrap;

/**
 * This is the model class for table "article".
 *
 * @property int $id
 * @property string $status
 * @property string $display_start
 * @property string $display_end
 * @property int $site_id
 * @property string $headline
 * @property string $subheadline
 * @property string $publication
 * @property string $media
 * @property string $summary
 * @property string $text
 * @property int $created_by
 * @property int $updated_by
 * @property int $activated_by
 * @property int $deactivated_by
 * @property int $deleted_by
 * @property string $created_at
 * @property string $updated_at
 * @property string $activated_at
 * @property string $deactivated_at
 * @property string $deleted_at
 *
 * @property Site $site
 */
class Article extends ActiveRecord implements ContentmetaInterface, FieldConfigInterface, EditViewInterface, IndexViewInterface
{
    use BeautifulModelTrait;
    use OptionTrait;
    use ContentmetaTrait;
    use TagTrait;
    //use HashIdPrimaryKeyTrait;

    public static string $searchModelClass = ArticleSearch::class;

    //region BeautifulModelTrait implementation
    /**
     * @inheritDoc
     */
    public static function editDataUrl($model): string
    {
        return Bootstrap::$id.'/article';
    }

    /**
     * @inheritDoc
     */
    public static function editDataIcon($model): string
    {
        return 'text-subject';
    }

    /**
     * @inheritDoc
     */
    public static function editDataPlural($model): string
    {
        return Yii::t('fafcms-blogmanager', 'Posts');
    }

    /**
     * @inheritDoc
     */
    public static function editDataSingular($model): string
    {
        return Yii::t('fafcms-blogmanager', 'Post');
    }

    /**
     * @inheritDoc
     */
    public static function extendedLabel($model, bool $html = true, array $params = []): string
    {
        $extendedLabel = trim(($model['headline'] ?? ''));

        if ($html) {
            $extendedLabel .= '<br><small><b>';
        }

        $extendedLabel .= trim(($model['subheadline'] ?? ''));

        if ($html) {
            $extendedLabel .= '</b></small>';
        }

        return $extendedLabel;
    }
    //endregion BeautifulModelTrait implementation

    //region OptionTrait implementation
    /**
     * @inheritDoc
     */
    public static function getOptions(bool $empty = true, array $select = null, array $sort = null, array $where = null, array $joinWith = null, string $emptyLabel = null): array
    {
        $options = Yii::$app->dataCache->map(
            self::class,
            array_merge([
                self::tableName().'.id',
                self::tableName().'.headline',
                self::tableName().'.subheadline',
                self::tableName().'.publication'
            ], $select??[]),
            'id',
            function ($item) {
                return self::extendedLabel($item);
            },
            null,
            $sort??[self::tableName().'.headline' => SORT_ASC, self::tableName().'.subheadline' => SORT_ASC],
            $where,
            $joinWith
        );

        return OptionTrait::getOptions($empty, $select, $sort, $where, $joinWith, $emptyLabel) + $options;
    }
    //endregion OptionTrait implementation

    //region ContentmetaInterface implementation
    /**
     * @return string
     */
    public static function contentmetaName(): string
    {
        return 'Article';
    }

    /**
     * @return string
     */
    public static function contentmetaId(): string
    {
        return 'id';
    }

    /**
     * @return string
     */
    public static function contentmetaSiteId(): string
    {
        return 'site_id';
    }
    //endregion ContentmetaInterface implementation

    //region FieldConfigInterface implementation
    public function getFieldConfig(): array
    {
        return [
            'id' => [
                'type' => TextInput::class,
                'options' => [
                    'disabled' => true
                ]
            ],
            'headline' => [
                'type' => TextInput::class,
            ],
            'subheadline' => [
                'type' => TextInput::class,
            ],
            'publication' => [
                'type' => DateTimePicker::class,
            ],
            'status' => [
                'type' => DropDownList::class,
                'items' => $this->attributeOptions()['status']
            ],
            'display_start' => [
                'type' => DateTimePicker::class,
            ],
            'display_end' => [
                'type' => DateTimePicker::class,
            ],
            'site_id' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->attributeOptions()['site_id'],
                'relationClassName' => Site::class,
            ],
            'media' => [
                'type' => FileSelect::class,
                'mediatypes' => ['image', 'video']
            ],
            'summary' => [
                'type' => Yii::$app->getModule(SettingmanagerBootstrap::$id)->getSetting('enable_tiny_mce_article_summary', Yii::$app->getModule(SettingmanagerBootstrap::$id)->getSetting('enable_tiny_mce_article', Yii::$app->getModule(SettingmanagerBootstrap::$id)->getSetting('enable_tiny_mce', false))) ? TinyMce::class : AceEditor::class,
            ],
            'text' => [
                'type' => Yii::$app->getModule(SettingmanagerBootstrap::$id)->getSetting('enable_tiny_mce_article_text', Yii::$app->getModule(SettingmanagerBootstrap::$id)->getSetting('enable_tiny_mce_article', Yii::$app->getModule(SettingmanagerBootstrap::$id)->getSetting('enable_tiny_mce', false))) ? TinyMce::class : AceEditor::class,
            ],
        ];
    }
    //endregion FieldConfigInterface implementation

    //region EditViewInterface implementation
    public static function editView(): array
    {
        return [
            'default' => [
                'tab-1' => [
                    'class' => \fafcms\fafcms\items\Tab::class,
                    'settings' => [
                        'label' => Yii::t('fafcms-core', 'Master data'),
                    ],
                    'contents' => [
                        'row-1' => [
                            'class' => \fafcms\fafcms\items\Row::class,
                            'contents' => [
                                'column-1' => [
                                    'class' => \fafcms\fafcms\items\Column::class,
                                    'settings' => [
                                        'm' => 6,
                                    ],
                                    'contents' => [
                                        'master-data-card' => [
                                            'class' => \fafcms\fafcms\items\Card::class,
                                            'settings' => [
                                                'title' => Yii::t('fafcms-core', 'Master data'),
                                                'icon' => 'id-card',
                                            ],
                                            'contents' => [
                                                [
                                                    'class' => \fafcms\fafcms\items\FormField::class,
                                                    'settings' => [
                                                        'field' => 'headline',
                                                    ],
                                                ],
                                                [
                                                    'class' => \fafcms\fafcms\items\FormField::class,
                                                    'settings' => [
                                                        'field' => 'subheadline',
                                                    ],
                                                ],
                                                [
                                                    'class' => \fafcms\fafcms\items\FormField::class,
                                                    'settings' => [
                                                        'field' => 'publication',
                                                    ],
                                                ],
                                            ]
                                        ],
                                    ]
                                ],
                                'column-2' => [
                                    'class' => \fafcms\fafcms\items\Column::class,
                                    'settings' => [
                                        'm' => 5,
                                    ],
                                    'contents' => [
                                        'access-restrictions-card' => [
                                            'class' => \fafcms\fafcms\items\Card::class,
                                            'settings' => [
                                                'title' => Yii::t('fafcms-core', 'Access restrictions'),
                                                'icon' => 'timetable',
                                            ],
                                            'contents' => [
                                                [
                                                    'class' => \fafcms\fafcms\items\FormField::class,
                                                    'settings' => [
                                                        'field' => 'status',
                                                    ],
                                                ],
                                                [
                                                    'class' => \fafcms\fafcms\items\FormField::class,
                                                    'settings' => [
                                                        'field' => 'display_start',
                                                    ],
                                                ],
                                                [
                                                    'class' => \fafcms\fafcms\items\FormField::class,
                                                    'settings' => [
                                                        'field' => 'display_end',
                                                    ],
                                                ],
                                                [
                                                    'class' => \fafcms\fafcms\items\FormField::class,
                                                    'settings' => [
                                                        'field' => 'site_id',
                                                    ],
                                                ],
                                            ]
                                        ],
                                    ]
                                ]
                            ],
                        ],
                        'row-2' => [
                            'class' => \fafcms\fafcms\items\Row::class,
                            'contents' => [
                                'column-1' => [
                                    'class' => \fafcms\fafcms\items\Column::class,
                                    'settings' => [
                                        'm' => 6
                                    ],
                                    'contents' => [
                                        'content-card' => [
                                            'class' => \fafcms\fafcms\items\Card::class,
                                            'settings' => [
                                                'title' => Yii::t('fafcms-core', 'Media'),
                                                'icon' => 'image-outline',
                                            ],
                                            'contents' => [
                                                [
                                                    'class' => \fafcms\fafcms\items\FormField::class,
                                                    'settings' => [
                                                        'field' => 'media',
                                                        'hideLabel' => true
                                                    ],
                                                ],
                                            ]
                                        ],
                                    ]
                                ],
                                'column-2' => [
                                    'class' => \fafcms\fafcms\items\Column::class,
                                    'settings' => [
                                        'm' => 6
                                    ],
                                    'contents' => [
                                        'content-card' => [
                                            'class' => \fafcms\fafcms\items\Card::class,
                                            'settings' => [
                                                'title' => Yii::t('fafcms-core', 'Summary'),
                                                'icon' => 'text-short',
                                            ],
                                            'contents' => [
                                                [
                                                    'class' => \fafcms\fafcms\items\FormField::class,
                                                    'settings' => [
                                                        'field' => 'summary',
                                                        'hideLabel' => true
                                                    ],
                                                ],
                                            ]
                                        ],
                                    ]
                                ],
                            ],
                        ],
                        'row-3' => [
                            'class' => \fafcms\fafcms\items\Row::class,
                            'contents' => [
                                'column-1' => [
                                    'class' => \fafcms\fafcms\items\Column::class,
                                    'contents' => [
                                        'content-card' => [
                                            'class' => \fafcms\fafcms\items\Card::class,
                                            'settings' => [
                                                'title' => Yii::t('fafcms-core', 'Content'),
                                                'icon' => 'card-text-outline',
                                            ],
                                            'contents' => [
                                                [
                                                    'class' => \fafcms\fafcms\items\FormField::class,
                                                    'settings' => [
                                                        'field' => 'text',
                                                        'hideLabel' => true
                                                    ],
                                                ],
                                            ]
                                        ],
                                    ]
                                ],
                            ],
                        ]
                    ]
                ],
            ],
        ];
    }
    //endregion EditViewInterface implementation

    //region IndexViewInterface implementation
    public static function indexView(): array
    {
        return [
            'default' => [
                [
                    'class' => \fafcms\fafcms\items\DataColumn::class,
                    'settings' => [
                        'field' => 'id',
                        'sort' => 1
                    ],
                ],
                [
                    'class' => \fafcms\fafcms\items\DataColumn::class,
                    'settings' => [
                        'field' => 'status',
                        'sort' => 2
                    ],
                ],
                [
                    'class' => \fafcms\fafcms\items\DataColumn::class,
                    'settings' => [
                        'field' => 'media',
                        'sort' => 3,
                    ],
                ],
                [
                    'class' => \fafcms\fafcms\items\DataColumn::class,
                    'settings' => [
                        'field' => 'headline',
                        'sort' => 4,
                        'link' => true
                    ],
                ],
                [
                    'class' => \fafcms\fafcms\items\DataColumn::class,
                    'settings' => [
                        'field' => 'subheadline',
                        'sort' => 5,
                    ],
                ],
                [
                    'class' => \fafcms\fafcms\items\DataColumn::class,
                    'settings' => [
                        'field' => 'publication',
                        'sort' => 6,
                    ],
                ],
                [
                    'class' => \fafcms\fafcms\items\DataColumn::class,
                    'settings' => [
                        'field' => 'display_start',
                        'sort' => 7,
                    ],
                ],
                [
                    'class' => \fafcms\fafcms\items\DataColumn::class,
                    'settings' => [
                        'field' => 'display_end',
                        'sort' => 8,
                    ],
                ],
                [
                    'class' => \fafcms\fafcms\items\ActionColumn::class,
                ],
            ]
        ];
    }
    //endregion IndexViewInterface implementation

    public function loadDefaultValues($skipIfSet = true): self
    {
        parent::loadDefaultValues($skipIfSet);

        if (!$skipIfSet || $this->publication === null) {
            $dateTime = new DateTime('now', new DateTimeZone(Yii::$app->formatter->defaultTimeZone));
            $dateTime->setTime($dateTime->format('H'), round(((int)$dateTime->format('i') + 15) / 5) * 5, 0);
            $this->publication = $dateTime->format('Y-m-d H:i:s');
        }

        if (!$skipIfSet || $this->site_id === null) {
            $this->site_id = Yii::$app->getModule(SettingmanagerBootstrap::$id)->getSetting('blog_post_default_site_id', null);
        }

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public static function prefixableTableName(): string
    {
        return '{{%article}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return array_merge(parent::rules(), [
            ['publication', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'publication', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            ['display_start', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'display_start', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            ['display_end', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'display_end', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            [['status', 'summary', 'text'], 'string'],
            [['display_start', 'display_end', 'publication', 'created_at', 'updated_at', 'activated_at', 'deactivated_at', 'deleted_at'], 'safe'],
            [['site_id', 'headline', 'publication'], 'required'],
            [['created_by', 'updated_by', 'activated_by', 'deactivated_by', 'deleted_by'], 'integer'],
            //[['site_id'], HashIdValidator::class],
            [['site_id'], 'integer'],
            [['headline', 'subheadline', 'media'], 'string', 'max' => 255],
            [['site_id'], 'exist', 'skipOnError' => true, 'targetClass' => Site::className(), 'targetAttribute' => ['site_id' => 'id']],
        ]);
    }

    public function attributeOptions()
    {
        return [
            'status' => [
                'active' => Yii::t('fafcms-blogmanager', 'Active'),
                'inactive' => Yii::t('fafcms-blogmanager', 'Inactive')
            ],
            'site_id' => Site::getOptions(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return array_merge(parent::attributeLabels(), [
            'id' => Yii::t('fafcms-blogmanager', 'ID'),
            'status' => Yii::t('fafcms-blogmanager', 'Status'),
            'display_start' => Yii::t('fafcms-blogmanager', 'Display start'),
            'display_end' => Yii::t('fafcms-blogmanager', 'Display end'),
            'site_id' => Yii::t('fafcms-blogmanager', 'Site template'),
            'headline' => Yii::t('fafcms-blogmanager', 'Headline'),
            'subheadline' => Yii::t('fafcms-blogmanager', 'Subheadline'),
            'publication' => Yii::t('fafcms-blogmanager', 'Publication'),
            'media' => Yii::t('fafcms-blogmanager', 'Media'),
            'summary' => Yii::t('fafcms-blogmanager', 'Summary'),
            'text' => Yii::t('fafcms-blogmanager', 'Text'),
            'created_by' => Yii::t('fafcms-blogmanager', 'Created By'),
            'updated_by' => Yii::t('fafcms-blogmanager', 'Updated By'),
            'activated_by' => Yii::t('fafcms-blogmanager', 'Activated By'),
            'deactivated_by' => Yii::t('fafcms-blogmanager', 'Deactivated By'),
            'deleted_by' => Yii::t('fafcms-blogmanager', 'Deleted By'),
            'created_at' => Yii::t('fafcms-blogmanager', 'Created At'),
            'updated_at' => Yii::t('fafcms-blogmanager', 'Updated At'),
            'activated_at' => Yii::t('fafcms-blogmanager', 'Activated At'),
            'deactivated_at' => Yii::t('fafcms-blogmanager', 'Deactivated At'),
            'deleted_at' => Yii::t('fafcms-blogmanager', 'Deleted At'),
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSite()
    {
        return $this->hasOne(Site::className(), ['id' => 'site_id']);
    }
}
