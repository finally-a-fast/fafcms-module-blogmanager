<?php

namespace fafcms\blogmanager\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\BaseDataProvider;

/**
 * ArticleSearch represents the model behind the search form.
 */
class ArticleSearch extends Article
{
    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['id', 'created_by', 'updated_by', 'activated_by', 'deactivated_by', 'deleted_by'], 'integer'],
            [['status', 'media',  'publication', 'display_start', 'display_end', 'headline', 'subheadline', 'summary', 'text', 'title', 'description', 'slug', 'created_at', 'updated_at', 'activated_at', 'deactivated_at', 'deleted_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios(): array
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * @param array $params
     *
     * @return BaseDataProvider
     * @throws \yii\base\InvalidConfigException
     */
    public function search(array $params): BaseDataProvider
    {
        $query = Article::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['publication' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'publication' => $this->publication,
            'display_start' => $this->display_start,
            'display_end' => $this->display_end,
            'media' => $this->media,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
            'activated_by' => $this->activated_by,
            'deactivated_by' => $this->deactivated_by,
            'deleted_by' => $this->deleted_by,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'activated_at' => $this->activated_at,
            'deactivated_at' => $this->deactivated_at,
            'deleted_at' => $this->deleted_at,
        ]);

        $query->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'headline', $this->headline])
            ->andFilterWhere(['like', 'subheadline', $this->subheadline])
            ->andFilterWhere(['like', 'summary', $this->summary])
            ->andFilterWhere(['like', 'text', $this->text])/*
            ->andFilterWhere(['like', 'title', $this->contentmeta->title])
            ->andFilterWhere(['like', 'description', $this->contentmeta->description])
            ->andFilterWhere(['like', 'slug', $this->contentmeta->slug])*/;

        return $dataProvider;
    }
}
