<?php

namespace fafcms\blogmanager;

use fafcms\fafcms\components\FafcmsComponent;
use yii\base\Application;
use fafcms\helpers\abstractions\PluginBootstrap;
use fafcms\helpers\abstractions\PluginModule;
use yii\base\BootstrapInterface;
use Yii;
use yii\helpers\ArrayHelper;
use yii\i18n\PhpMessageSource;
use fafcms\blogmanager\models\Article;

class Bootstrap extends PluginBootstrap implements BootstrapInterface
{
    public static $id = 'fafcms-blogmanager';
    public static $tablePrefix = 'fafcms-blog_';

    protected function bootstrapTranslations(Application $app, PluginModule $module): bool
    {
        if (!isset($app->i18n->translations['fafcms-blogmanager'])) {
            $app->i18n->translations['fafcms-blogmanager'] = [
                'class' => PhpMessageSource::class,
                'basePath' => __DIR__ . '/messages',
                'forceTranslation' => true,
            ];
        }

        return true;
    }

    protected function bootstrapWebApp(Application $app, PluginModule $module): bool
    {
        $module->accessRules = array_merge([
            'article' => [
                [
                    'allow' => true,
                    'roles' => ['@'],
                ]
            ],
        ], $module->accessRules);

        $actionColumn = [
            'class' => $module->actionColumnClass,
            'visibleButtons' => [
                'view' => false,
                'update' => true,
                'delete' => true,
            ],
        ];

        if ($module->actionColumn === null) {
            $module->actionColumn = $actionColumn;
        }

        if (isset($app->fafcmsParser)) {
            $app->fafcmsParser->replacements = array_merge($app->fafcmsParser->replacements, require(__DIR__ . '/replacements.php'));

            $app->fafcmsParser->data['allArticles'] = function () {
                return ArrayHelper::index(Article::find()->all(), 'id');
            };
        }


        Yii::$app->view->addNavigationItems(
            FafcmsComponent::NAVIGATION_SIDEBAR_LEFT,
            [
                'project' => [
                    'items' => [
                        'website' => [
                            'items' => [
                                'posts' => [
                                    'after' => 'sites',
                                    'icon'  => Article::instance()->getEditData()['icon'],
                                    'label' => Article::instance()->getEditData()['plural'],
                                    'url'   => ['/' . Article::instance()->getEditData()['url'] . '/index'],
                                ],
                            ],
                        ],
                    ],
                ],
            ]
        );

        $app->fafcms->addBackendUrlRules(self::$id, [
            '<controller:[a-zA-Z0-9\-]+>/<action:[a-zA-Z0-9\-]+>/<id:[a-zA-Z0-9\-\_]+>' => '<controller>/<action>',
            '<controller:[a-zA-Z0-9\-]+>/<action:[a-zA-Z0-9\-]+>' => '<controller>/<action>',
        ]);

        return true;
    }
}
