<?php
/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2019 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-module-blogmanager/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-module-blogmanager
 * @see https://www.finally-a-fast.com/packages/fafcms-module-blogmanager/docs Documentation of fafcms-module-blogmanager
 * @since File available since Release 1.0.0
 */

namespace fafcms\blogmanager\controllers;

use fafcms\blogmanager\models\Article;
use fafcms\helpers\DefaultController;

/**
 * Class ArticleController
 * @package fafcms\blogmanager\controllers
 */
class ArticleController extends DefaultController
{
    public static $modelClass = Article::class;
}
